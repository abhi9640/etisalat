package mav.Etisalat.alltestpack;

import java.util.Map;

import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.etisalat.Test.TestSteps;

public class EtisalatTest extends EtisalatTestNGMethods {
	@Test(description = "BlockUnblockFlow ", dataProvider = "TestDataParallel")
	public static void BlockUnblockFlow(Map<String, String> brow, Map<String, String> data, ITestContext ctx)
			throws InterruptedException {
		TestSteps steps = new TestSteps();
		String page_name="BlockUnblockFlow";
		steps.BlockUnblockFlow(page_name);
	}
	}

