package com.etisalat.framework;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import mav.Etisalat.Driver.DriverFactory;

public class Base {
	public static String captureScreen() {
		String path;
		File trgtPath = null;
		try {
			WebDriver driverLoc = DriverFactory.getCurrentDriver();
			WebDriver augmentedDriver = new Augmenter().augment(driverLoc);
			File source = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
			path = ExReporter.reportPath + "/" + source.getName();
			// System.out.println(path);
			trgtPath = new File(path);

			FileUtils.copyFile(source, trgtPath);
			return source.getName();// trgtPath.getAbsolutePath();
		} catch (Exception e) {
			return "";
		}
		// return trgtPath.getAbsolutePath();

	}

	public static void waitForPageLoad() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		ExpectedCondition<Boolean> expect = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		Wait<WebDriver> wait = new WebDriverWait(driver, 120);
		try {
			wait.until(expect);
		} catch (Exception E) {
			E.printStackTrace();
			ExReporter.log(LogStatus.INFO, "Page Load Condition failed. Continuing with test");
		}
	}
}
