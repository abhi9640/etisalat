package com.etisalat.POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.etisalat.framework.ExReporter;
import com.etisalat.framework.TestData;
import com.relevantcodes.extentreports.LogStatus;

import mav.Etisalat.Driver.DriverFactory;


public class HomePage
{
	@FindBy(xpath="(//*[@class='btn btn-default ripple-effect'])[5]")        public WebElement ContactUs;
	@FindBy(xpath="(//*[@class='cross-nav-text body-small'])[5]")			 public WebElement Unblock;
	@FindBy(id="name")													     public WebElement	name;
	@FindBy(id="email")														 public WebElement email;
	@FindBy(id="phone1")													 public WebElement	Phone;
	@FindBy(id="url")													     public WebElement	url;
	@FindBy(id="reason")													 public WebElement reason;
	
	WebDriver driver = DriverFactory.getCurrentDriver();
	public void populateDetails()
	{
		try {
			
		name.sendKeys(TestData.getConfig("Name"));
		email.sendKeys(TestData.getConfig("Email"));
		Phone.sendKeys(TestData.getConfig("Phone"));
		url.sendKeys(TestData.getConfig("Url"));
		reason.sendKeys(TestData.getConfig("Reason"));
		ExReporter.log(LogStatus.PASS, "Sucessfully populated values");
		}
		catch(Exception e)
		{
			ExReporter.log(LogStatus.FAIL, "Unable be enter details");
		}
	}
	public void ClickContact()
	{
		try {
		ContactUs.click();
		ExReporter.log(LogStatus.PASS, "Sucessfully Clicked Contacus");
		}
		catch(Exception e)
		{
			ExReporter.log(LogStatus.FAIL, "Unable Clickcontact");
		}
	}
	public void ClickUnblock()
	{
		try
		{
		Unblock.click();
		ExReporter.log(LogStatus.PASS, "Sucessfully Clicked Unblock");
		}
		catch(Exception e)
		{
			ExReporter.log(LogStatus.FAIL, "Unable Click Unblock");
		}
	}
}
