package com.etisalat.Test;


import org.openqa.selenium.WebDriver;

import com.etisalat.framework.Base;
import com.etisalat.framework.ConfigProvider;
import com.etisalat.framework.ExReporter;
import com.etisalat.framework.ProjectConfig;
import com.relevantcodes.extentreports.LogStatus;

import mav.Etisalat.Driver.BrowserStackDriver;
import mav.Etisalat.Driver.DriverFactory;


public class LaunchUrl{
	static WebDriver driver;
	public static void loadHomePage() throws InterruptedException {
		if(ConfigProvider.getConfig("Browser").contains("BrowserStack")){
		driver = BrowserStackDriver.getNewDriver();
		}else {
		driver=DriverFactory.getCurrentDriver();
		}
		driver.get(ProjectConfig.getPropertyValue("url"));
		ExReporter.log(LogStatus.PASS, "Sucessfully launched application");
	}
}
