package com.etisalat.Test;


import com.etisalat.GalenFramework.GalenMethods;
import com.etisalat.POM.HomePage;
import com.etisalat.framework.ConfigProvider;
import com.etisalat.framework.ExReporter;
import com.etisalat.framework.ProjectConfig;
import com.etisalat.framework.TestData;
import com.relevantcodes.extentreports.LogStatus;

import mav.Etisalat.Driver.DriverFactory;

public class TestSteps
{
	public TestSteps() {
		initReport();
		logDetails();
		DriverFactory.driverInit();
	}
	public static void initReport() {
		try {
			String testName = ConfigProvider.getConfig("Testname") + "-" + TestData.getConfig("DataBinding");
			String desc = "";
			if (ProjectConfig.getPropertyValue("versionspecific").equals("true")
					|| ConfigProvider.getConfig("Platform").contains("BrowserStack"))
				desc = ConfigProvider.getConfig("Browser") + "-" + ConfigProvider.getConfig("Version");
			else
				desc = ConfigProvider.getConfig("Browser");
			ExReporter.initTest(testName, desc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void logDetails() {
		//System.out.println(ProjectConfig.getPropertyValue("url"));
		try {
			if (ConfigProvider.getConfig("Platform").equalsIgnoreCase("Browser"))
				ExReporter.log(LogStatus.INFO, "URL: " + ProjectConfig.getPropertyValue("url"));

			else
				ExReporter.log(LogStatus.INFO, "URL: " + ProjectConfig.getPropertyValue("murl"));
			ExReporter.log(LogStatus.INFO, "OS: " + ConfigProvider.getConfig("OS"));
			ExReporter.assignCatogory(ConfigProvider.getConfig("OS"));
			ExReporter.assignCatogory(ConfigProvider.getConfig("Testname"));

			if (ProjectConfig.getPropertyValue("versionspecific").equals("true")
					|| ConfigProvider.getConfig("Platform").contains("BrowserStack")) {
				ExReporter.log(LogStatus.INFO,
						"Browser: " + ConfigProvider.getConfig("Browser") + "-" + ConfigProvider.getConfig("Version"));
				ExReporter.assignCatogory(ConfigProvider.getConfig("Browser") + "-" + ConfigProvider.getConfig("Version"));
			} else {
				ExReporter.log(LogStatus.INFO, "Browser: " + ConfigProvider.getConfig("Browser"));
				ExReporter.assignCatogory(ConfigProvider.getConfig("Browser"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void BlockUnblockFlow(String page_name) throws InterruptedException
	{
		LaunchUrl.loadHomePage();
		GalenMethods.galenspec("header", "header", "header");
		/*HomePage hm=new HomePage();
		hm.ClickContact();
		hm.ClickUnblock();
		hm.populateDetails()*/;
		
	}
}
